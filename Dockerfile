FROM python:3.10.5-slim-buster

ENV LANG C.UTF-8
ARG PIP=pip3

ENV TZ=Europe/Paris

RUN apt-get update && apt-get install -y \
        python3-pip \
        git \
        curl

RUN pip install --upgrade pip

WORKDIR /app
COPY requirements.txt /app/
COPY educationenrichoccupations/   /app/educationenrichoccupations/
COPY setup.py /app/
COPY trigger_enrich_occupations.py /app/

RUN echo 'This line is only here to clear Openshift/Docker cache for jobtech packages. Take 2.'
RUN ${PIP} install --upgrade --no-cache-dir --ignore-installed git+https://gitlab.com/arbetsformedlingen/education/education-opensearch.git
RUN ${PIP} install -r requirements.txt

ENTRYPOINT [ "python" ]
CMD [ "/app/trigger_enrich_occupations.py" ]
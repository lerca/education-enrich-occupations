# Education enrich occupations

Project for enriching aggregated occupations with demanded competencies.


When running the following will happen:
1. Historical ads are downloaded from Minio (files have to be in ndjson-format, like .jsonl)
2. The ads are enriched with JobAd Enrichments API
3. The enriched result is added to each occupation
4. A complete list with enriched occupations are persisted in Opensearch

See 'the How to prepare ads data to enrich' section for preparation (e.g. ads from other years) before step 1.

## Installation in local dev environment
1. Choose your python environment
2. In a terminal, run:
```
pip install -r requirements.txt
```
3. Adjust environment variables in educationenrichoccupations/settings.py
4. To install as a python package, in a terminal, run:
```
python setup.py develop
```

## Run the enriching and persisting of occupations
Execute trigger_enrich_occupations.py to run locally.

For execution in other project, see example in trigger_enrich_occupations.py


## Docker

### Build
docker build -t educationenrichoccupations:latest .

### Run
docker run -d --env-file=docker.env --name educationenrichoccupations educationenrichoccupations

### Check log file
docker logs educationenrichoccupations --details -f

### Stop & remove image
docker stop educationenrichoccupations;docker rm educationenrichoccupations || true

Windows:
docker stop educationenrichoccupations && docker rm educationenrichoccupations || true

### Debug Docker
docker logs educationenrichoccupations --details -f
docker exec -t -i educationenrichoccupations /bin/bash
docker run -it --rm educationenrichoccupations:latest

### Mac, remove previous docker-builds + cointainers
docker system prune

# How to prepare ads data to enrich
1. Download historical ads file, e.g. from https://data.jobtechdev.se/annonser/historiska/index.html and unzip.
2. Make sure the ads files are unzipped and put into tools/ads_resources folder.
3. Convert the files from json format to ndjson format.
   1. Run convert_json_ads_to_ndjson if the whole year is represented in one year.
      Verify source and dest path. Dont change pattern in dest path.
   2. Run convert_json_ads_from_multiple_files_to_ndjson if the year is split 
      in multiple files (e.g. 20xxq1.json). Verify source and dest path. Dont change pattern in dest path.
4. Split ndjson files into 4 files by running convert_historical_ads_to_ndjson_parts. Verify source and dest path.
5. Upload files to minio. Verify env.variables for aws/s3. Run upload_files_to_minio.
   Note that files with same name will be overwritten.
6. The files are ready to be enriched from, see beginning of readme.
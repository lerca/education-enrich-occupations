from educationenrichoccupations.enrich_occupations_starter import EnrichOccupationsStarter


if __name__ == '__main__':
    enrich_occupations_starter = EnrichOccupationsStarter()
    enrich_occupations_starter.start()
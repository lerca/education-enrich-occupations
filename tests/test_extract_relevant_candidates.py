import pytest
import jmespath
from educationenrichoccupations.enrich_occupations import *


currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

log = logging.getLogger(__name__)

@pytest.fixture(scope="module")
def enriched_occupations():
    return load_json_file(currentdir + 'resources/enriched-occupations-to-store-minified-2016-2021.json')

# @pytest.mark.skip(reason="Temporarily disabled")
def test_ensure_terms_are_not_unique(enriched_occupations):

    enriched_occupations_to_store = parse_most_frequent_enriched_candidates(enriched_occupations)
    assert len(enriched_occupations_to_store) > 0
    enriched_occupation = enriched_occupations_to_store[0]
    relevant_enriched_candidates = jmespath.search('occupation.relevant_enriched_candidates', enriched_occupation)
    relevant_enriched_candidates_occ = jmespath.search('occupations', relevant_enriched_candidates)
    relevant_enriched_candidates_comp = jmespath.search('competencies', relevant_enriched_candidates)
    relevant_enriched_candidates_traits = jmespath.search('traits', relevant_enriched_candidates)
    relevant_enriched_candidates_geos = jmespath.search('geos', relevant_enriched_candidates)

    assert len(set(relevant_enriched_candidates_occ)) < len(relevant_enriched_candidates_occ)
    assert len(set(relevant_enriched_candidates_comp)) < len(relevant_enriched_candidates_comp)
    assert len(set(relevant_enriched_candidates_traits)) < len(relevant_enriched_candidates_traits)
    assert len(set(relevant_enriched_candidates_geos)) < len(relevant_enriched_candidates_geos)





def load_json_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = json.load(file)
        return data

if __name__ == '__main__':
    pytest.main([os.path.realpath(__file__), '-svv', '-ra'])

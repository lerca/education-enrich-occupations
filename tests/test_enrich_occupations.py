import ndjson
import pytest

from educationenrichoccupations.enrich_occupations import *


currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

log = logging.getLogger(__name__)

@pytest.fixture(scope="module")
def historical_ads():
    return load_ndjson_file(currentdir + 'resources/historical_ads_2020_beta1_1_percent_ensure_ascii_part_1_of_4.jsonl')



# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_doc_headline_input(historical_ads):
    test_ad = get_test_ad_by_id('24433095', historical_ads)
    headline_input = get_doc_headline_input(test_ad)
    # print(headline_input)
    # Make sure that the occupation label was found in the json-structure.
    assert headline_input.lower().startswith('sjuksköterska, grundutbildad')


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_doc_headline_input2(historical_ads):
    test_ad = get_test_ad_by_id('23981973', historical_ads)
    headline_input = get_doc_headline_input(test_ad)
    # print(headline_input)
    # Make sure that the occupation label was found in the json-structure.
    assert headline_input.lower().startswith('lärare i grundskolan')


def get_test_ad_by_id(id, dicts):
    return next((item for item in dicts if item["id"] == id), None)


def load_ndjson_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = ndjson.load(file)
        return data

if __name__ == '__main__':
    pytest.main([os.path.realpath(__file__), '-svv', '-ra'])

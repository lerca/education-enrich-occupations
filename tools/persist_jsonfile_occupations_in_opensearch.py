import logging
import os

import ndjson
from education_opensearch.opensearch_store import OpensearchStore

from educationenrichoccupations import settings
from educationenrichoccupations.enrich_occupations import parse_most_frequent_enriched_candidates

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


def load_ndjson_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = ndjson.load(file)
        return data


def save_enriched_occupations_from_file_in_repository():
    enriched_aggregated_occupations = load_ndjson_file(currentdir + settings.OCCUPATIONS_RELATIVE_FILE_PATH)

    enriched_occupations_to_store = parse_most_frequent_enriched_candidates(enriched_aggregated_occupations)

    # log.info(json.dumps(enriched_occupations_to_store))

    save_in_repository(enriched_occupations_to_store)


def save_in_repository(enriched_occupations_to_store):
    os_store = OpensearchStore()
    index_name = os_store.start_new_save(settings.OCCUPATIONS_ALIAS_NAME,
                                         mappings=settings.ENRICHED_OCCUPATIONS_INDEX_CONFIG)
    log.info('Will save enriched occupations in index_name: %s' % index_name)
    os_store.save_items_in_repository(enriched_occupations_to_store)
    os_store.create_or_update_alias_for_index(index_name, settings.OCCUPATIONS_ALIAS_NAME)


save_enriched_occupations_from_file_in_repository()

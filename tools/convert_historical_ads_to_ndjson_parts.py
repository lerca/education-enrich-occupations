import os
import json
import ndjson
import math
from educationenrichoccupations.helpers import grouper

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

# source_file = currentdir + 'original_historical_ads/historical_ads_2020_beta1_1_percent.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2020_beta1_1_percent_ensure_ascii_part_%s_of_%s.jsonl'

# source_file = currentdir + 'original_historical_ads/historical_ads_2016_beta1.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2016_beta1_ensure_ascii_part_%s_of_%s.jsonl'

# source_file = currentdir + 'original_historical_ads/historical_ads_2017_beta1.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2017_beta1_ensure_ascii_part_%s_of_%s.jsonl'

# source_file = currentdir + 'original_historical_ads/historical_ads_2018_beta1.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2018_beta1_ensure_ascii_part_%s_of_%s.jsonl'

# source_file = currentdir + 'original_historical_ads/historical_ads_2019_beta1.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2019_beta1_ensure_ascii_part_%s_of_%s.jsonl'

# source_file = currentdir + 'original_historical_ads/historical_ads_2020_beta1.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2020_beta1_ensure_ascii_part_%s_of_%s.jsonl'

# source_file = currentdir + 'original_historical_ads/historical_ads_2021_beta1.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2021_beta1_ensure_ascii_part_%s_of_%s.jsonl'

source_file = currentdir + 'original_historical_ads/2022.jsonl'
dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2022_beta1_ensure_ascii_part_%s_of_%s.jsonl'

def load_ndjson_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = ndjson.load(file)
        return data


def write_records_to_file(json_src, filepath):
    print('Writing records to file: %s' % filepath)
    file = open(filepath, 'w', encoding='utf-8')
    for record in json_src:
        file.write((json.dumps(record, ensure_ascii=True)).strip() + '\n')
    file.close()


def convert_historical_ads():
    json_src = load_ndjson_file(source_file)

    nr_of_parts = 4
    historical_ads_size = len(json_src)

    nr_of_items_per_batch = math.ceil(historical_ads_size / nr_of_parts)

    ad_batches = grouper(nr_of_items_per_batch, json_src)

    for i, batch in enumerate(ad_batches):
        dest_file_part = dest_file % ((i + 1), nr_of_parts)
        # print(dest_file_part)
        # print(len(batch))
        write_records_to_file(batch, dest_file_part)

## Convert ads file in json format into a ndjson file.
convert_historical_ads()

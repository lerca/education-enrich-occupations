import logging
import os
from os import listdir
from os.path import isfile, join

from tools.minio_uploader import MinioUploader

# Folder containing the files to upload, relative to this script, for example 'files_to_upload_to_minio' for
# the absolute path '/Users/theuser/Documents/git/education-enrich-occupations/tools/files_to_upload_to_minio/'
RELATIVE_LOCAL_FOLDER_PATH = 'files_to_upload_to_minio'
# RELATIVE_LOCAL_FOLDER_PATH = 'files_to_upload_to_minio_kll'

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


def upload_files():
    minio_uploader = MinioUploader()

    upload_folder_path = currentdir + RELATIVE_LOCAL_FOLDER_PATH
    local_filepaths = [join(upload_folder_path, f) for f in listdir(upload_folder_path) if
                       isfile(join(upload_folder_path, f))]
    minio_uploader.upload_files(local_filepaths)


upload_files()

import logging
import os
import sys
import threading

import boto3
from boto3.s3.transfer import S3Transfer
from botocore.client import Config

from educationenrichoccupations import settings


class MinioUploader(object):
    s3_url = settings.S3_URL
    s3_bucket_name = settings.S3_BUCKET_NAME
    aws_access_key = settings.AWS_ACCESS_KEY
    aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)

        self.s3_client = boto3.client('s3',
                                      endpoint_url=self.s3_url,
                                      aws_access_key_id=self.aws_access_key,
                                      aws_secret_access_key=self.aws_secret_access_key,
                                      config=Config(signature_version='s3v4'),
                                      region_name='us-east-1')

    def upload_files(self, local_filepaths):
        self.log.info('Will upload files to bucket %s with the following filepaths:\n%s' % (
        self.s3_bucket_name, '\n'.join(local_filepaths)))

        transfer = S3Transfer(self.s3_client)

        for local_filepath in local_filepaths:
            # print(local_filepath)
            filename = os.path.basename(local_filepath)
            self.log.info('Uploading file: %s' % filename)
            # bucket.upload_file(local_filepath, filename, callback=ProgressPercentage(local_filepath))
            transfer.upload_file(local_filepath, self.s3_bucket_name, filename,
                                 callback=ProgressPercentage(local_filepath))


class ProgressPercentage(object):
    def __init__(self, filename):
        self._filename = filename
        self._size = float(os.path.getsize(filename))
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        # To simplify we'll assume this is hooked up
        # to a single filename.
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            sys.stdout.write(
                "\r%s  %s / %s  (%.2f%%)" % (
                    self._filename, self._seen_so_far, self._size,
                    percentage))
            sys.stdout.flush()

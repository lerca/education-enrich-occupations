import json
import logging

import boto3
from botocore.client import Config
from tempfile import TemporaryFile
import threading
import sys
import re
import os
from educationenrichoccupations import settings


class MinioDownloader(object):
    s3_url = settings.S3_URL
    s3_bucket_name = settings.S3_BUCKET_NAME
    aws_access_key = settings.AWS_ACCESS_KEY
    aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)

        self.s3_client = boto3.client('s3',
                                      endpoint_url=self.s3_url,
                                      aws_access_key_id=self.aws_access_key,
                                      aws_secret_access_key=self.aws_secret_access_key,
                                      config=Config(signature_version='s3v4'),
                                      region_name='us-east-1')

    def download_ads_from_file(self, ad_src_file_name):
        with TemporaryFile() as data:
            response = self.s3_client.head_object(Bucket=self.s3_bucket_name, Key=ad_src_file_name)
            filesize = response['ContentLength']
            self.log.info('Starting download for file %s with filesize: %s' % (ad_src_file_name, filesize))
            self.s3_client.download_fileobj(self.s3_bucket_name, ad_src_file_name, data,
                                            Callback=ProgressPercentage(ad_src_file_name, filesize))
            # self.s3_client.download_fileobj(self.s3_bucket_name, ad_src_file_name, data)
            self.log.info('Downloaded file %s' % ad_src_file_name)

            data.seek(0)
            for line in data.readlines():
                decoded_line = line.decode("utf-8").strip()
                # print(line)
                # print(decoded_line)
                yield json.loads(decoded_line)

    def get_filtered_filenames_in_bucket(self):
        object_list = self.s3_client.list_objects_v2(
            Bucket=self.s3_bucket_name
        )
        RE_WANTED_FILE_PATTERN = re.compile(settings.S3_AD_SRC_FILE_NAME_PATTERN, re.UNICODE)

        fileobjects = object_list['Contents']

        filtered_filenames = [filename['Key'] for filename in fileobjects if
                              RE_WANTED_FILE_PATTERN.match(filename['Key'])]

        return filtered_filenames

    # def download_ads_from_file(self, ad_src_file_name):
    #     line_counter = 0
    #     self.log.info('Trying to read file %s from Minio' % ad_src_file_name)
    #     response = self.s3.Object(self.s3_bucket_name, ad_src_file_name).get()
    #     body = response['Body']
    #
    #     self.log.info('Streaming ads from file: %s' % ad_src_file_name)
    #
    #     for line in body.iter_lines():
    #         decoded_line = line.decode("utf-8")
    #         line_counter += 1
    #         if line_counter % 1000 == 0:
    #             self.log.info('Downloaded %s lines from file: %s' % (line_counter, ad_src_file_name))
    #         yield json.loads(decoded_line)

    # def download_ads(self):
    #     for ad_src_file_name in self.ad_src_file_names:
    #         line_counter = 0
    #         self.log.info('Trying to read file %s from Minio' % ad_src_file_name)
    #         response = self.s3.Object(self.s3_bucket_name, ad_src_file_name).get()
    #         body = response['Body']
    #
    #         self.log.info('Streaming ads from file: %s' % ad_src_file_name)
    #
    #         for line in body.iter_lines():
    #             decoded_line = line.decode("utf-8")
    #             line_counter += 1
    #             if line_counter % 1000 == 0:
    #                 self.log.info('Downloaded %s lines from file: %s' % (line_counter, ad_src_file_name))
    #             yield json.loads(decoded_line)


class ProgressPercentage(object):
    def __init__(self, filename, filesize):
        self._filename = filename
        self._size = float(filesize)
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        # To simplify we'll assume this is hooked up
        # to a single filename.
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            sys.stdout.write(
                "\rDownloading %s  %s / %s  (%.2f%%)" % (
                    self._filename, self._seen_so_far, self._size,
                    percentage))
            sys.stdout.flush()

# md = MinioDownloader()
# print(md.get_filtered_filenames_in_bucket())

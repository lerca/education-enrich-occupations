import os

S3_URL = os.getenv('S3_URL', 'https://minio.arbetsformedlingen.se')
S3_BUCKET_NAME = os.getenv('S3_BUCKET_NAME', 'kll-test')
AWS_ACCESS_KEY = os.getenv('AWS_ACCESS_KEY', 'kll-test')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY', 'your Minio password here')

#  Use pattern that matches for example: historical_ads_2016_beta1_ensure_ascii_part_1_of_4.jsonl
S3_AD_SRC_FILE_NAME_PATTERN = os.getenv('S3_AD_SRC_FILE_NAME_PATTERN', 'historical_ads_\d{4}.*_part_\d.*\.jsonl')
# S3_AD_SRC_FILE_NAME_PATTERN = os.getenv('S3_AD_SRC_FILE_NAME_PATTERN', 'historical_ads_2020_beta1_1_percent_ensure_ascii_part_\d.*\.jsonl')

OCCUPATIONS_ALIAS_NAME = os.getenv('OCCUPATIONS_ALIAS_NAME', 'enriched-occupations')

LANG_CODES_CACHE_RELATIVE_FILE_PATH = os.getenv('LANG_CODES_CACHE_RELATIVE_FILE_PATH', 'resources/jobad_id_lang_code_cache_2016_2021.json')

OCCUPATIONS_RELATIVE_FILE_PATH = os.getenv('OCCUPATIONS_FILE_PATH', '../educationenrichoccupations/enriched-occupations-2016-2021.jsonl')

OCCUPATIONS_SAVE_RESULT_IN_FILE_ONLY = os.getenv('OCCUPATIONS_SAVE_RESULT_IN_FILE_ONLY', 'false').lower() == 'true'
# OCCUPATIONS_SAVE_RESULT_IN_FILE_ONLY = True

ENRICH_CALLS_PARALLELISM = int(os.getenv('ENRICH_CALLS_PARALLELISM', '8'))
# JAE_API_URL = os.getenv('JAE_API_URL', 'https://jobad-enrichments-api.jobtechdev.se')
JAE_API_URL = os.getenv('JAE_API_URL', 'https://jobad-enrichments-test-api.jobtechdev.se')
JAE_API_TIMEOUT_SECONDS = int(os.getenv('JAE_API_TIMEOUT_SECONDS', '60'))

ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 19200)
ES_USER = os.getenv("ES_USER", "admin")
ES_PWD = os.getenv("ES_PWD", "admin")
ES_USE_SSL = os.getenv('ES_USE_SSL', 'false').lower() == 'true'
ES_VERIFY_CERTS = os.getenv('ES_VERIFY_CERTS', 'false').lower() == 'true'

# ES_HOST=search-jobtech-opensearch-7zrtdhvq4feysy3peubhzsweai.eu-central-1.es.amazonaws.com
# ES_PORT=443
# ES_USER=your_user_here
# ES_PWD=your_password_here
# ES_USE_SSL=true
# ES_VERIFY_CERTS=false

ENRICHED_OCCUPATIONS_INDEX_CONFIG = {
    "settings": {
        "analysis": {
            "char_filter": {
                "mapping_char_filter": {
                    "type": "mapping",
                    "mappings": [
                        "- => _"
                    ]
                }
            },
            "analyzer": {
                "char_filter_analyzer": {
                    "tokenizer": "standard",
                    "char_filter": [
                        "mapping_char_filter"
                    ],
                    "filter": [
                        "lowercase"
                    ]
                }
            },
            "normalizer": {
                "occupationnormalizer": {
                    "type": "custom",
                    "char_filter": [],
                    "filter": ["lowercase"]
                }
            }
        }
    },
    "mappings": {
        "properties": {
            "id": {
                "type": "keyword",
            },
            "occupation": {
                "properties": {
                    "label": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "normalizer": "occupationnormalizer",
                                "ignore_above": 256
                            }
                        }
                    },
                    "occupation_group": {
                        "properties": {
                            "label": {
                                "type": "text",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "normalizer": "occupationnormalizer",
                                        "ignore_above": 256
                                    }
                                }
                            }
                        }
                    },
                    "enriched_candidates": {
                        "properties": {
                            "competencies": {
                                "type": "text",
                                "analyzer": "char_filter_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "geos": {
                                "type": "text",
                                "analyzer": "char_filter_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "occupations": {
                                "type": "text",
                                "analyzer": "char_filter_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "traits": {
                                "type": "text",
                                "analyzer": "char_filter_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            }
                        }
                    },
                    "relevant_enriched_candidates": {
                        "properties": {
                            "competencies": {
                                "type": "text",
                                "analyzer": "char_filter_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "geos": {
                                "type": "text",
                                "analyzer": "char_filter_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "occupations": {
                                "type": "text",
                                "analyzer": "char_filter_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "traits": {
                                "type": "text",
                                "analyzer": "char_filter_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

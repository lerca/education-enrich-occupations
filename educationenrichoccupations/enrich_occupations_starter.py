import logging

from educationenrichoccupations.enrich_occupations import enrich_occupations_and_persist


class EnrichOccupationsStarter(object):

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)

    def start(self):
        self.log.info('Starting enrich occupations and persist process...')
        enrich_occupations_and_persist()
